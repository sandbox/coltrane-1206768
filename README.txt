Variables
--------------------------------------------------------------------------------
Theme Variables creates variables from Drupal objects, so the exact variable
name depends on the object. However, the module sets portions of the variable
depending on it's intended use.

prefix
suffix
label
value
list
  Example: $field_organizers_list
  PHP Array list of values set in the field organizers. Each value is also
  available as direct variables under view_[0-X].
view_[0-X]
  Example: $field_organizers_view_2
  Third value of the field organizers list (list starts at 0).

API
--------------------------------------------------------------------------------

hook_themevars_variables()

hook_themevars_[object]()
example: hook_themevars_node()


